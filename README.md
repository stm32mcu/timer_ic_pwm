# Timer_IC_PWM

Demo showing how to measure PWM period and duty cycle with a timer in PWM input mode

TIM1_CH1 is setup for measuring a PWM generated in software on the same chip.
No interrupts are needed, its all timer hardware.

TIM1 configuration. The prescaler was chosen SystemCoreClock()/1000-1 (counting milliseconds), counter period to maximum.
Depending on the task, you might want to use a 32-bit counter.

![TIM1 configuration](TIM1_PWM_input.png)